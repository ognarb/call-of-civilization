/**
 *  This file is part of 'Call Of Civilization'.
 *
 *  Copyright Carl-Lucien Schwan <schwancarl@protonmail.com>
 *
 *  'Call Of Civilization' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  'Call Of Civilization' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'Call Of Civilization'. If not, see <https://www.gnu.org/licenses/>.
 */


use piston::input::*;
use opengl_graphics::GlGraphics;
use map::Map;
use graphics::Context;

static CAMERA_SPEED: f64 = 10.0;

pub struct App {
    pub gl: GlGraphics,
    pub map: Map,
    pub context: AppContext,
}

#[derive(Copy, Clone)]
pub struct AppContext {
    pub zoom: f64,
    pub pos: (f64, f64),
    pub size: (u32, u32),
    pub map_size: (usize, usize),
    cursor: (f64, f64),
}

pub trait GameObject {
    fn render(&self, ctxt: &Context, gl: &mut GlGraphics, app_ctxt: &AppContext);
    fn update(&mut self, _: f64) {
        // By default do nothing in the update function
    }
}

impl App {
    pub fn render(&mut self, args: &RenderArgs) {
        use graphics::*;
        let map = &self.map;
        let context = &self.context;
        self.gl.draw(args.viewport(), | c, gl| {
            clear([1.0; 4], gl);
            map.render(&c, gl,  context);
        });
    }

    pub fn event<E: GenericEvent>(&mut self, e: &E) {
        e.mouse_cursor(|x, y| self.context.update_cursor((x,y)));
        e.resize(|w, h| {
            self.context.size = (w, h);
            self.context.stabilize_camera();
        });
        e.mouse_scroll(|_dx, dy| self.context.change_zoom(dy));
        match e.press_args() {
            Some(Button::Keyboard(Key::J)) => self.context.change_zoom(-0.5),
            Some(Button::Keyboard(Key::K)) => self.context.change_zoom(0.5),
            Some(Button::Keyboard(Key::Left)) => self.context.camera_left(),
            Some(Button::Keyboard(Key::Right)) => self.context.camera_right(),
            Some(Button::Keyboard(Key::Down)) => self.context.camera_down(),
            Some(Button::Keyboard(Key::Up)) => self.context.camera_up(),
            Some(Button::Keyboard(_)) => (),
            Some(Button::Mouse(_)) => (),
            Some(Button::Hat(_)) => (),
            Some(Button::Controller(_)) => (),
            None => (),
        }
        self.context.get_field_size();
    }
}

static ZOOM_SPEED : f64 = 5.0;

impl AppContext {
    pub fn new(map_size: (usize, usize)) -> Self {
        AppContext {
            zoom: 1.0,
            pos: (0.0, 0.0),
            size: (0, 0),
            map_size,
            cursor: (0.0, 0.0),
        }
    }

    pub fn update_cursor(&mut self, cursor: (f64, f64)) {
        self.cursor = cursor;
        let cursor_relative = (cursor.0 / self.size.0 as f64, cursor.1 / self.size.1 as f64);
        if cursor_relative.0 < 0.1 {
            self.camera_left();
        } else if cursor_relative.0 > 0.9 {
            self.camera_right();
        }
        if cursor_relative.1 > 0.9 {
            self.camera_down();
        } else if cursor_relative.1 < 0.1 {
            self.camera_up();
        }

    }

    pub fn camera_down(&mut self) {
        self.pos.1 -= CAMERA_SPEED;
        self.stabilize_camera();
    }

    pub fn camera_up(&mut self) {
        self.pos.1 += CAMERA_SPEED;
        self.stabilize_camera();
    }

    pub fn camera_left(&mut self) {
        self.pos.0 += CAMERA_SPEED;
        self.stabilize_camera();
    }

    pub fn camera_right(&mut self) {
        self.pos.0 -= CAMERA_SPEED;
        self.stabilize_camera();
    }

    pub fn stabilize_camera(&mut self) {
        let field_size = self.get_field_size();
        if self.pos.1 > 0.0 {
            self.pos.1 = 0.0;
        }
        if self.pos.0 > 0.0 {
            self.pos.0 = 0.0;
        }
        let i = self.size.1 as f64 - self.zoom * field_size * self.map_size.1 as f64;
        if (self.pos.1 as f64) < i {
            self.pos.1 = i;
        }
        let i = self.size.0 as f64 - self.zoom * field_size * self.map_size.0 as f64;
        if (self.pos.0 as f64) < i {
            self.pos.0 = i;
        }
    }


    pub fn get_field_size(&self) -> f64 {
        let width_size = self.size.0 as f64 / self.map_size.0 as f64;
        let height_size = self.size.1 as f64 / self.map_size.1 as f64;
        if width_size > height_size {
            width_size
        } else {
            height_size
        }
    }

    fn change_zoom(&mut self, dy: f64) {

        let world_coord = self.get_world_coordinate(self.cursor);
        self.zoom += dy / ZOOM_SPEED;
        if self.zoom < 1.0 {
            self.zoom = 1.0;
        } else if self.zoom > 4.0 {
            self.zoom = 4.0;
        }
        let screen_coord = self.get_screen_coordinate(world_coord);
        self.pos.0 += self.cursor.0 - screen_coord.0;
        self.pos.1 += self.cursor.1 - screen_coord.1;

        self.stabilize_camera();
    }

    fn get_world_coordinate(&self, mouse: (f64, f64)) -> (f64, f64) {
        ((mouse.0 - self.pos.0) / self.get_field_size() / self.zoom, (mouse.1 - self.pos.1) / self.get_field_size() / self.zoom)
    }

    fn get_screen_coordinate(&self, world: (f64, f64)) -> (f64, f64) {
        (self.pos.0 + (world.0 * self.get_field_size() * self.zoom), self.pos.1 + (world.1 * self.get_field_size() * self.zoom))
    }


}

#[cfg(test)]
mod tests {
    use super::AppContext;

    #[test]
    fn change_zoom_zoom_in_test_simple() {
        let mut context = AppContext::new((10, 10));
        context.size = (10, 10);
        context.cursor = (5.0, 5.0);
        context.change_zoom(5.0);
        assert!((context.zoom - 2.0).abs() < 0.01);
        assert!((context.pos.0 + 2.5).abs() < 0.01);
        assert!((context.pos.1 + 2.5).abs() < 0.01);
    }

    /*

    #[test]
    fn change_zoom_zoom_out_test_simple() {
        let mut context = AppContext::new((10, 10));
        context.size = (10, 10);
        context.cursor = (5.0, 5.0);
        context.change_zoom(5.0);
        assert!((context.zoom - 2.0).abs() < 0.01);
        assert!((context.pos.0 + 2.5).abs() < 0.01);
        assert!((context.pos.1 + 2.5).abs() < 0.01);
        context.change_zoom(-5.0);
        assert!((context.zoom - 1.0).abs() < 0.01);
        assert!((context.pos.0 + 0.0).abs() < 0.01);
        assert!((context.pos.1 + 0.0).abs() < 0.01);
    }

    #[test]
    fn change_zoom_zoom_in_test_complex() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.cursor = (50.0, 50.0);
        context.change_zoom(5.0);
        assert!((context.zoom - 2.0).abs() < 0.01);
        assert!((context.pos.0 + 25.0).abs() < 0.01);
        assert!((context.pos.1 + 25.0).abs() < 0.01);
    }

    #[test]
    fn change_zoom_zoom_out_test_complex() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.cursor = (50.0, 50.0);
        context.change_zoom(5.0);
        assert!((context.zoom - 2.0).abs() < 0.01);
        assert!((context.pos.0 + 25.0).abs() < 0.01);
        assert!((context.pos.1 + 25.0).abs() < 0.01);
        context.change_zoom(-5.0);
        assert!((context.zoom - 1.0).abs() < 0.01);
        assert!((context.pos.0 + 0.0).abs() < 0.01);
        assert!((context.pos.1 + 0.0).abs() < 0.01);
    }

    #[test]
    fn change_zoom_zoom_in_test_complex_2() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.cursor = (50.0, 50.0);
        context.change_zoom(2.5);
        assert!((context.zoom - 1.5).abs() < 0.01);
        assert!((context.pos.0 + 16.66).abs() < 0.01);
        assert!((context.pos.1 + 16.66).abs() < 0.01);
    }

    #[test]
    fn change_zoom_zoom_in_test_cursor_up_left() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.cursor = (0.0, 0.0);
        context.change_zoom(5.0);
        assert!((context.zoom - 2.0).abs() < 0.01);
        assert!(context.pos.0.abs() < 0.01);
        assert!(context.pos.1.abs() < 0.01);
    }

    #[test]
    fn change_zoom_zoom_in_test_cursor_bottom_right() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.cursor = (100.0, 100.0);
        context.change_zoom(5.0);
        assert!((context.zoom - 2.0).abs() < 0.01);
        assert!((context.pos.0 + 50.0).abs() < 0.01);
        assert!((context.pos.1 + 50.0).abs() < 0.01);
        context.change_zoom(5.0);
        assert!((context.zoom - 3.0).abs() < 0.01);
        assert!((context.pos.0 + 66.66).abs() < 0.01);
        assert!((context.pos.1 + 66.66).abs() < 0.01);
        context.change_zoom(5.0);
        assert!((context.zoom - 4.0).abs() < 0.01);
        assert!((context.pos.0 + 75.0).abs() < 0.01);
        assert!((context.pos.1 + 75.0).abs() < 0.01);
    }
    
    #[test]
    fn change_zoom_zoom_in_test_cursor_bottom_right_1() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.cursor = (95.0, 95.0);
        context.change_zoom(5.0);
        println!("{}", context.pos.1);
        assert!((context.zoom - 2.0).abs() < 0.01);
        assert!((context.pos.0 + 50.0).abs() < 0.01);
        assert!((context.pos.1 + 50.0).abs() < 0.01);
        context.change_zoom(5.0);
        assert!((context.zoom - 3.0).abs() < 0.01);
        assert!((context.pos.0 + 66.66).abs() < 0.01);
        assert!((context.pos.1 + 66.66).abs() < 0.01);
        context.change_zoom(5.0);
        assert!((context.zoom - 4.0).abs() < 0.01);
        assert!((context.pos.0 + 75.0).abs() < 0.01);
        assert!((context.pos.1 + 75.0).abs() < 0.01);
    }*/

    #[test]
    fn test_world_to_screen_1() {
        let mut context = AppContext::new((100, 100));
        context.size = (100, 100);
        context.cursor = (95.0, 95.0);
        let screen = context.get_screen_coordinate((95.0, 95.0));
        assert!((screen.0 - context.cursor.0).abs() < 0.01);
        assert!((screen.0 - context.cursor.1).abs() < 0.01);
    }

    #[test]
    fn test_world_to_screen_2() {
        let mut context = AppContext::new((100, 100));
        context.size = (10, 10);
        context.cursor = (9.5, 9.5);
        let screen = context.get_screen_coordinate((95.0, 95.0));
        assert!((screen.0 - context.cursor.0).abs() < 0.01);
        assert!((screen.0 - context.cursor.1).abs() < 0.01);
    }

    #[test]
    fn test_world_to_screen_3() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.zoom = 2.0;
        context.pos = (-25.0, -25.0);
        context.cursor = (5.0, 5.0);
        let screen = context.get_screen_coordinate((50.0, 50.0));
        println!("screen {}", screen.0);
        assert!((screen.0 - context.cursor.0).abs() < 0.01);
        assert!((screen.0 - context.cursor.1).abs() < 0.01);
    }

    #[test]
    fn test_screen_to_world_1() {
        let mut context = AppContext::new((100, 100));
        context.size = (100, 100);
        context.cursor = (95.0, 95.0);
        let world = context.get_world_coordinate(context.cursor);
        assert!((world.0 - context.cursor.0).abs() < 0.01);
        assert!((world.0 - context.cursor.1).abs() < 0.01);
    }

    #[test]
    fn test_screen_to_world_2() {
        let mut context = AppContext::new((10, 10));
        context.size = (100, 100);
        context.cursor = (95.0, 95.0);
        let world = context.get_world_coordinate(context.cursor);
        assert!((world.0 - context.cursor.0 / 10.0).abs() < 0.01);
        assert!((world.0 - context.cursor.1 / 10.0).abs() < 0.01);
    }
}
