/**
 *  This file is part of 'Call Of Civilization'.
 *
 *  Copyright Carl-Lucien Schwan <schwancarl@protonmail.com>
 *
 *  'Call Of Civilization' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  'Call Of Civilization' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'Call Of Civilization'. If not, see <https://www.gnu.org/licenses/>.
 */

use std::fmt;
use std::iter::FromIterator;
use noise::{Turbulence, Add, Perlin, Seedable, utils::*};
use field::Field;
use field::FieldType;
use std::time::{SystemTime, UNIX_EPOCH};

use piston::input::*;
use opengl_graphics::GlGraphics;
use app::AppContext;
use app::GameObject;
use graphics::Context;

pub struct Map {
    noise_map : NoiseMap,
    pub fields : Vec<Vec<Field>>,
}

impl Map {
    pub fn new(width: usize, height: usize) -> Map {
        let raw_val2biome:Vec<(f64, FieldType)> = vec![(0.4, FieldType::Ocean), (0.95, FieldType::Plain), (1.0, FieldType::Mountain)];
        Map::new_with_biomes(width, height, raw_val2biome)
    }

    pub fn new_with_biomes(width: usize, height: usize, raw_val2biome:Vec<(f64, FieldType)>) -> Map{
        let perlin = Perlin::new().set_seed(
            SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards")
            .as_secs() as u32);
        let perlin2 = Perlin::new().set_seed(
            SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards")
            .as_secs() as u32);
        let exp = Turbulence::new(&perlin2).set_frequency(50.0).set_power(0.5);
        let add = Add::new(&perlin, &exp);

        let noise_map = PlaneMapBuilder::new(&add).set_size(width, height).build();

        let mut values : Vec<f64> = Vec::new();
        for x in 0..noise_map.size().0 {
            for y in 0..noise_map.size().1 {
                values.push(noise_map.get_value(x, y));
            }
        }

        values.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());

        let val2biome = Vec::from_iter(raw_val2biome.into_iter().map(|(a ,b)| (values[((values.len()-1) as f64 * a) as usize], b)));
        
        let mut fields : Vec<Vec<Field>> = Vec::new();

        for x in 0..noise_map.size().0 {
            let mut column : Vec<Field> = Vec::new();
            for y in 0..noise_map.size().1 {
                let curr = noise_map.get_value(x, y);
                for (i,j) in &val2biome {
                    if curr <= *i {
                        column.push(Field {
                            field_type: *j,
                            x,
                            y
                        });
                        break;
                    }
                }
            }
            fields.push(column);
        }

        // todo transform all ocean field near a plain in FieldType::Beach

        Map {
            noise_map,
            fields,
        }
    }

    pub fn get_size(&self) -> (usize, usize) {
        (self.fields.len(), self.fields[0].len())
    }
}

impl GameObject for Map {
    fn render(&self, ctxt: &Context, gl: &mut GlGraphics, app_ctxt: &AppContext) {
        for column in self.fields.iter() {
            for field in column.iter() {
                field.render(ctxt, gl, app_ctxt);
            }
        }

    }
}

impl fmt::Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{},{} vs {},{}", self.noise_map.size().0, self.noise_map.size().1, self.fields.len(), self.fields[0].len())?;
        for y in 0..self.fields[0].len() {
            for x in 0..self.fields.len() {
                write!(f, "{} ", self.fields[x][y].field_type() as u8)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

