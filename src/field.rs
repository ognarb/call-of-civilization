/**
 *  This file is part of 'Call Of Civilization'.
 *
 *  Copyright Carl-Lucien Schwan <schwancarl@protonmail.com>
 *
 *  'Call Of Civilization' is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  'Call Of Civilization' is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with 'Call Of Civilization'. If not, see <https://www.gnu.org/licenses/>.
 */

use opengl_graphics::GlGraphics;
use app::AppContext;
use std::fmt;
use app::GameObject;
use graphics::{context::Context, rectangle, Transformed};

#[derive(Copy, Clone)]
pub enum FieldType {
    Ocean,
    Beach,
    Plain,
    Mountain,
}

#[derive(Copy, Clone)]
pub struct Field {
    pub field_type : FieldType,
    pub x: usize,
    pub y: usize,
}

static FIELD_SIZE : f64 = 40.0;

impl Field {
    pub fn field_type(&self) -> FieldType {
        self.field_type
    }

    fn x(&self) -> usize {
        self.x
    }

    fn y(&self) -> usize {
        self.y
    }

    fn size(&self) -> f64 {
        FIELD_SIZE
    }
}

impl GameObject for Field {
    fn render(&self, ctxt: &Context, q: &mut GlGraphics, app_ctxt: &AppContext) {
        let rect = rectangle::square(0.0, 0.0, app_ctxt.get_field_size() * app_ctxt.zoom);

        let transform = ctxt
            .trans(app_ctxt.pos.0, app_ctxt.pos.1)
            .zoom(app_ctxt.zoom)
            .trans(
                (self.x as f64) * app_ctxt.get_field_size(),
                (self.y as f64) * app_ctxt.get_field_size())
            .transform
        ;

        let color = match self.field_type {
            FieldType::Ocean => [0.0, 0.0, 1.0, 1.0],
            FieldType::Beach => [0.0, 0.0, 0.9, 1.0],
            FieldType::Plain => [0.0, 1.0, 0.0, 1.0],
            FieldType::Mountain => [0.9, 0.9, 0.9, 1.0],
        };

        rectangle(color, rect, transform, q);

    }
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.field_type as u8)?;
        Ok(())
    }
}
